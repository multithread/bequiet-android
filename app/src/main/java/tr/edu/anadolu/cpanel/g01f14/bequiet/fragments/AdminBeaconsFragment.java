package tr.edu.anadolu.cpanel.g01f14.bequiet.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tr.edu.anadolu.cpanel.g01f14.bequiet.ApplicationController;
import tr.edu.anadolu.cpanel.g01f14.bequiet.ChangeBeaconsDetailActivity;
import tr.edu.anadolu.cpanel.g01f14.bequiet.GetAllBeaconsWithIdActivity;
import tr.edu.anadolu.cpanel.g01f14.bequiet.R;
import tr.edu.anadolu.cpanel.g01f14.bequiet.adapters.AdminBeaconListAdapter;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.MyBeacons;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.staticFields;

/**
 * Created by Tuncer on 24.12.2014.
 */
public class AdminBeaconsFragment extends Fragment {


    ArrayList<MyBeacons> beacons;
    ListView list;
    AdminBeaconListAdapter beacons_adapter;
    TextView tw;
    Button btn_insert;
    int user_id;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        Log.d("userid", "" + "AdminBeaconsFragment");
        if(getArguments()!=null){
             user_id=getArguments().getInt("UserId");
            Log.d("userid", "adminbeacons" + user_id);
            Toast.makeText(getActivity().getApplicationContext(),"user id : "+ user_id , Toast.LENGTH_SHORT).show();
        }

        View view = inflater.inflate(R.layout.admin_beacons_fragment, container, false);

        beacons = new ArrayList<MyBeacons>();


        btn_insert = (Button) view.findViewById(R.id.btn_insert);
      /*  final CharSequence[] ar = new String[beacons.size()];
        for(int i=0; i<beacons.size();i++){
            ar[i]="deneme";
        }*/

        btn_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




               Toast.makeText(getActivity().getApplicationContext(),"insert adminbeaconsFragment",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), AddBeaconsDetailActivity.class);
                // MyBeacons b =new MyBeacons(beacons.get(i).getBeaconID(), beacons.get(i).getMessage(),beacons.get(i).getPlace());
                intent.putExtra(staticFields.EXTRAS_BEACON_ID,user_id );
                startActivity(intent);

            }
        });



       getBeaconsAndId();

       beacons_adapter = new AdminBeaconListAdapter(getActivity().getApplicationContext(),beacons);



        list = (ListView) view.findViewById(R.id.list);
        tw = (TextView) view.findViewById(R.id.connect);
        list.setEmptyView(tw);
        list.setAdapter(beacons_adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
             //   Intent intent = new Intent(getActivity(), ChangeBeaconsDetailActivity.class);
                Intent intent = new Intent(getActivity(), GetAllBeaconsWithIdActivity.class);
                // MyBeacons b =new MyBeacons(beacons.get(i).getBeaconID(), beacons.get(i).getMessage(),beacons.get(i).getPlace());
                intent.putExtra(staticFields.EXTRAS_BEACON_ID, beacons.get(i).getBeaconID());
                startActivity(intent);
            }
        });


        return view;
    }



    void getBeaconsAndId(){

        final ProgressDialog mProgressDialog;

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setTitle("Service Connection");
        mProgressDialog.setMessage("Reaching...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.show();

        String URL1 = "http://bequiet.azurewebsites.net/api/user?UserId="+user_id;
        JsonArrayRequest req = new JsonArrayRequest(URL1, new Response.Listener<JSONArray>(){

            @Override
            public void onResponse(JSONArray response) {
                try {


                    for (int i = 0; i < response.length(); i++) {
                        JSONObject obj = response.getJSONObject(i);
                        MyBeacons beacon = new MyBeacons(obj.getInt("Major"), obj.getString("Name"), "Click for Update!");
                        beacons.add(beacon);
                    }
                    beacons_adapter.notifyDataSetChanged();
                    mProgressDialog.dismiss();



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        ApplicationController.getInstance().addToRequestQueue(req);


    }


}
