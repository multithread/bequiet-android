package tr.edu.anadolu.cpanel.g01f14.bequiet.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.widget.Toast;

import tr.edu.anadolu.cpanel.g01f14.bequiet.MainActivity;
import tr.edu.anadolu.cpanel.g01f14.bequiet.R;

/**
 * Created by Tuncer on 14.12.2014.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceClickListener {

    public static final String KEY_PREF_ENABLE = "pref_key_enable";
    public static final String KEY_PREF_BEQUIET_VERSION = "pref_key_about_bequiet_version";
    public static final String KEY_PREF_BEQUIET_WHAT_IS = "pref_key_about_bequiet_what_is";
    public static final String KEY_PREF_MULTITHREAD_WHO_WE_ARE = "pref_key_about_multithread_who_we_are";

    private long time1;
    private long time2;
    private int clickCount;

    private SharedPreferences sharedPreferences;

    public SettingsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).registerOnSharedPreferenceChangeListener(this);

        sharedPreferences = getPreferenceManager().getSharedPreferences();

        boolean isAdmin = sharedPreferences.getBoolean("isAdmin", false);

        if (!isAdmin) {
            findPreference(KEY_PREF_BEQUIET_VERSION).setOnPreferenceClickListener(this);
        }

        findPreference(KEY_PREF_BEQUIET_WHAT_IS).setOnPreferenceClickListener(this);
        findPreference(KEY_PREF_MULTITHREAD_WHO_WE_ARE).setOnPreferenceClickListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(KEY_PREF_ENABLE)) {
            SwitchPreference enablePref = (SwitchPreference) findPreference(KEY_PREF_ENABLE);

            if (enablePref.isChecked()) {
//                todo: BeQuiet Enabled

            } else {
//                todo: BeQuiet Disabled

            }
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        String key = preference.getKey();

        if (key.equals(KEY_PREF_BEQUIET_VERSION) && !sharedPreferences.getBoolean("isAdmin", false)) {
            time1 = time2;
            time2 = System.currentTimeMillis();

            if (time2 - time1 < 2000) {
                clickCount++;
            } else {
                clickCount = 1;
            }

            if (clickCount < 5) {
                String s = "BeQuiet!!!";
                Toast.makeText(getActivity().getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            } else if (clickCount < 10) {
                String s = String.format("Click %d %s to enable Admin Panel", 10 - clickCount, (10 - clickCount) == 1 ? "time" : "times");
                Toast.makeText(getActivity().getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            } else {
                String s = "Now you can reach Admin Panel";
                Toast.makeText(getActivity().getApplicationContext(), s, Toast.LENGTH_SHORT).show();

//                todo: Admin Panel Enabled

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("isAdmin", true);
                editor.apply();

                ((MainActivity) getActivity()).enableAdminItemInNavigatiıonDrawer();
            }

            return true;
        } else if (key.equals(KEY_PREF_BEQUIET_WHAT_IS)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
            builder.setTitle("BeQuiet");
            builder.setMessage(getResources().getString(R.string.bequiet_description));
            builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getActivity().getApplicationContext(), "Thanks for using BeQuiet", Toast.LENGTH_LONG).show();
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else if (key.equals(KEY_PREF_MULTITHREAD_WHO_WE_ARE)) {

//            todo: düzenlenecek

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setIcon(getResources().getDrawable(R.drawable.simge));
            builder.setTitle("MULTI-THREAD");
            builder.setMessage(getResources().getString(R.string.multithread_description));
            builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getActivity().getApplicationContext(), "DO NOT FORGET HAVE A LOOK AT BeQuite BEFORE KNOCK THE DOOR!!", Toast.LENGTH_LONG).show();
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else {

        }

        return false;
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        View rootView = inflater.inflate(R.layout.settings_fragment, container, false);
//
//        return rootView;
//
//    }
}
