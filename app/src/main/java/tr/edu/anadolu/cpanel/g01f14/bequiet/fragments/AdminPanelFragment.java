package tr.edu.anadolu.cpanel.g01f14.bequiet.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import tr.edu.anadolu.cpanel.g01f14.bequiet.ApplicationController;
import tr.edu.anadolu.cpanel.g01f14.bequiet.R;

/**
 * Created by Tuncer on 24.12.2014.
 */
public class AdminPanelFragment extends Fragment {

    EditText userName;
    EditText password;

    Button LogIn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.admin_panel_fragment, container, false);

        userName = (EditText) view.findViewById(R.id.username_edittex);
        password = (EditText) view.findViewById(R.id.password_edittex);

        LogIn = (Button) view.findViewById(R.id.logın_button);

        LogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ProgressDialog mProgressDialog;

                mProgressDialog = new ProgressDialog(getActivity());
                // Set progressdialog title
                mProgressDialog.setTitle("Service Connection");
                // Set progressdialog message
                mProgressDialog.setMessage("Reaching...");
                mProgressDialog.setIndeterminate(false);
                // Show progressdialog
                mProgressDialog.show();


                final String URL = "http://bequiet.azurewebsites.net/api/loginapi?Email=" +userName.getText().toString()+"&Password="+password.getText().toString();
                JsonObjectRequest req = new JsonObjectRequest(URL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                            Log.d("gelen",response.toString());

                                try {
                                    if(response.getString("value").equalsIgnoreCase("OK")){
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("UserId",response.getInt("UserId"));
                                        mProgressDialog.dismiss();
                                        FragmentManager fragmentManager = getFragmentManager();
                                        Log.d("UserId",""+response.getInt("UserId"));
                                        // FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction ft =  fragmentManager.beginTransaction();
                                        AdminBeaconsFragment abf = new AdminBeaconsFragment();
                                        ft.setCustomAnimations(R.animator.slide_in_left ,R.animator.slide_in_right);

                                        abf.setArguments(bundle);
                                        ft.replace(R.id.frame_container, abf);

                                        ft.commit();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getActivity().getApplicationContext(),"yanlış",Toast.LENGTH_SHORT).show();
                                }


                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());

                    }
                });
                ApplicationController.getInstance().addToRequestQueue(req);


            }
        });

        return view;
    }

    public static String POST(String url){
        InputStream inputStream = null;
        String result = "";
        try {

            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            String json = "";

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("Email","a@b.com");
            jsonObject.accumulate("Password","123456");


            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();


            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        // 11. return result
        return result;
    }


    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {


            return POST(urls[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            Toast.makeText(getActivity().getBaseContext(), "DATA SEND", Toast.LENGTH_LONG).show();
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
