package tr.edu.anadolu.cpanel.g01f14.bequiet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tr.edu.anadolu.cpanel.g01f14.bequiet.ApplicationController;
import tr.edu.anadolu.cpanel.g01f14.bequiet.R;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.MyBeacons;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.staticFields;

/**
 * Created by Tuncer on 11.1.2015.
 */
public class AddBeaconsDetailActivity extends Activity {
    ProgressDialog progressDialog;
    Button choose,send,ChooseStartTime,ChooseEndTime;
    ArrayList<MyBeacons> beacons;
    CharSequence[] ar;

    EditText CourseName,InstructorName,Description;
    int user_id;
    String choosen_id;

    String ChooseStartTime_s,ChooseEndTime_s;
    DatePicker datePicker;
    TimePicker timePicker;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_beacon_content);
        CourseName = (EditText) findViewById(R.id.courseName);
        InstructorName = (EditText) findViewById(R.id.instructorName);
        Description = (EditText) findViewById(R.id.description);

        ChooseStartTime = (Button)findViewById(R.id.ChooseStartTime);
        ChooseEndTime = (Button)findViewById(R.id.ChooseEndTime);

        choose = (Button) findViewById(R.id.choose);
        send = (Button) findViewById(R.id.Send);
        beacons = new ArrayList<MyBeacons>();

        Intent intent = getIntent();
        user_id = intent.getIntExtra(staticFields.EXTRAS_BEACON_ID,0);


        getBeaconsAndId();


        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder builder = new AlertDialog.Builder(AddBeaconsDetailActivity.this);
                builder.setTitle("Choose your selection");
                builder.setItems(ar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        choosen_id = ""+beacons.get(item).getBeaconID();
                        choose.setText(beacons.get(item).getBeaconID()+"ses");
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        final View dialogView1 = View.inflate(AddBeaconsDetailActivity.this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog1 = new AlertDialog.Builder(AddBeaconsDetailActivity.this).create();

        dialogView1.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                datePicker= (DatePicker) dialogView1.findViewById(R.id.date_picker);
                timePicker= (TimePicker) dialogView1.findViewById(R.id.time_picker);

                int y= timePicker.getCurrentHour();
                String x = ""+timePicker.getCurrentHour();
                if (y<10)
                    x ="0" + x;
                ChooseStartTime_s = datePicker.getYear()+"-"+((datePicker.getMonth())+1)+"-"+datePicker.getDayOfMonth()+"T"+ x+":"+timePicker.getCurrentMinute()+":00";
                //ChooseStartTime_s =datePicker.getMonth()+""+datePicker.getDayOfMonth()+""+datePicker.getYear()+"T"+ timePicker.getCurrentHour()+""+timePicker.getCurrentMinute();
                alertDialog1.dismiss();

                Toast.makeText(getApplication().getApplicationContext(),ChooseStartTime_s+"",Toast.LENGTH_SHORT ).show();
            }});
        ChooseStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alertDialog1.setView(dialogView1);
                alertDialog1.show();
            }
        });

        final View dialogView_endTime = View.inflate(AddBeaconsDetailActivity.this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog_endTime = new AlertDialog.Builder(AddBeaconsDetailActivity.this).create();

        dialogView_endTime.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker datePicker = (DatePicker) dialogView_endTime.findViewById(R.id.date_picker);
                TimePicker timePicker = (TimePicker) dialogView_endTime.findViewById(R.id.time_picker);
                //timePicker.setIs24HourView(DateFormat.is24HourFormat(AddBeaconsDetailActivity.this));

              /*  Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute());*/
                int y= timePicker.getCurrentHour();
                String x = ""+timePicker.getCurrentHour();
                if (y<10)
                    x ="0" + x;
               // ChooseEndTime_s =datePicker.getMonth()+""+datePicker.getDayOfMonth()+""+datePicker.getYear()+"T"+ timePicker.getCurrentHour()+""+timePicker.getCurrentMinute();
                ChooseEndTime_s =datePicker.getYear()+"-"+((datePicker.getMonth())+1)+"-"+datePicker.getDayOfMonth()+"T"+ x+":"+timePicker.getCurrentMinute()+":00";
                alertDialog_endTime.dismiss();

                Toast.makeText(getApplication().getApplicationContext(),ChooseEndTime_s+"",Toast.LENGTH_SHORT ).show();
            }});


        ChooseEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog_endTime.setView(dialogView_endTime);
                alertDialog_endTime.show();



            }
        });




        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String CourseName_s = CourseName.getText().toString();
                    String InstructorName_s = InstructorName.getText().toString();
                    String Description_s = Description.getText().toString();
                    insert(choosen_id,CourseName_s,Description_s,ChooseStartTime_s,InstructorName_s,ChooseEndTime_s);
                    //
                }catch(Exception e) {
                    Toast.makeText(getApplication().getApplicationContext(),"Blank Areas!",Toast.LENGTH_SHORT ).show();
                }


            }
        });


    }


    void getBeaconsAndId(){

        String URL1 = "http://bequiet.azurewebsites.net/api/user?UserId="+user_id;
        JsonArrayRequest req = new JsonArrayRequest(URL1, new Response.Listener<JSONArray>(){

            @Override
            public void onResponse(JSONArray response) {
                try {


                    for (int i = 0; i < response.length(); i++) {
                        JSONObject obj = response.getJSONObject(i);
                        MyBeacons beacon = new MyBeacons(obj.getInt("Major"), obj.getString("Name"), obj.getString("Contents"));
                        beacons.add(beacon);
                    }


                    ar = new String[beacons.size()];
                    Log.d("gelem", "onPostExecute " + beacons.size());
                    for (int i = 0; i < beacons.size(); i++) {
                        ar[i] = beacons.get(i).getBeaconID() + "- " + beacons.get(i).getPlace();
                        Log.d("gelem", "onPostExecute ar " + ar[i]);

                    }



                    Log.d("gelem", response.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        ApplicationController.getInstance().addToRequestQueue(req);


    }



    public void insert(final String Id,final String CourseName,final String Description,final String StartDateTime,final String InstructorName,final String EndDateTime){
//final String Id,final String CourseName,final String Description,final String StartDateTime,final String InstructorName,final String EndDateTime
        final String URL = "http://bequiet.azurewebsites.net/api/contentapi/";

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest putRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Toast.makeText(getApplicationContext(), " inserted", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Toast.makeText(getApplicationContext(), "Error!!", Toast.LENGTH_SHORT).show();
            }
        }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Id",Id);
                params.put("CourseName", CourseName);
                params.put("Description", Description);
                params.put("EndDateTime", EndDateTime);
                params.put("InstructorName", InstructorName);
                params.put("StartDateTime", StartDateTime);
                params.put("BeaconId", Id);

                Log.d("yolla",CourseName);
                Log.d("yolla",Description);
                Log.d("yolla",EndDateTime);
                Log.d("yolla",InstructorName);
                Log.d("yolla",StartDateTime);
                Log.d("yolla",Id);


                return params;
            }

        };

        queue.add(putRequest);


    }


}

