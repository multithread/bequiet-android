package tr.edu.anadolu.cpanel.g01f14.bequiet.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import tr.edu.anadolu.cpanel.g01f14.bequiet.R;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.MyBeacons;

/**
 * Created by Tuncer on 25.12.2014.
 */
public class AdminBeaconListAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<MyBeacons> myBeaconList;

    public AdminBeaconListAdapter(Context context,ArrayList<MyBeacons> myBeaconList) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.myBeaconList = myBeaconList;

    }

    @Override
    public int getCount() {
        return myBeaconList.size();
    }

    @Override
    public Object getItem(int i) {
        return myBeaconList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MyBeaconDetail holder;
        View v= view;
        if(v == null){
            v = inflater.inflate(R.layout.admin_panel_beacon_list_items, viewGroup,false);
            holder = new MyBeaconDetail();
            holder.myBeaconPlace = (TextView)v.findViewById(R.id.myBeaconPlace);
            holder.myBeaconMessage= (TextView)v.findViewById(R.id.myBeaconMessage);
            holder.myBeaconImage = (ImageView)v.findViewById(R.id.imageView1);
            holder.relativeLayout = (RelativeLayout)v.findViewById(R.id.relativeLayout);
            v.setTag(holder);
        }
        else
            holder = (MyBeaconDetail) v.getTag();

        if (i % 2 == 0) {
            holder.relativeLayout.setBackgroundResource(R.drawable.layout_shape);
        } else {
            holder.relativeLayout.setBackgroundResource(R.drawable.layout_shape_pass);
        }

        holder.myBeaconImage.setImageResource(R.drawable.beacon_gray);
        holder.myBeaconMessage.setText(myBeaconList.get(i).getMessage());
        holder.myBeaconPlace.setText(myBeaconList.get(i).getPlace());

        return v;
    }

    private static class MyBeaconDetail
    {
        private RelativeLayout relativeLayout;
        private TextView myBeaconPlace;
        private TextView myBeaconMessage;
        private ImageView myBeaconImage;

    }
}
