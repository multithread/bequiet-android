package tr.edu.anadolu.cpanel.g01f14.bequiet;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tr.edu.anadolu.cpanel.g01f14.bequiet.fragments.AddBeaconsDetailActivity;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.MyBeacons;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.staticFields;

/**
 * Created by Tuncer on 25.12.2014.
 */
public class ChangeBeaconsDetailActivity extends Activity {



    int id;
    String CourseName_s;
    String Description_s;
    String EndDateTime_s;
    String InstructorName_s;
    String StartDateTime_s;
    int BeaconId;


    EditText CourseName,InstructorName,Description;
    TextView txt_start_date_time,txt_end_date_time;
    Button start_date,end_date;

    Button Back, Forward;
    int beacon_id;

    DatePicker datePicker;
    TimePicker timePicker;
    String ChooseStartTime_s,ChooseEndTime_s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_beacon_detail_activity);

        Intent intent = getIntent();
        //beacon_id = intent.getIntExtra(staticFields.EXTRAS_BEACON_ID,0);
        id = intent.getIntExtra("Id", 1);
        CourseName_s = intent.getStringExtra("CourseName");
        Description_s = intent.getStringExtra("Description");
        EndDateTime_s = intent.getStringExtra("EndDateTime");
        InstructorName_s = intent.getStringExtra("InstructorName");
        StartDateTime_s = intent.getStringExtra("StartDateTime");
        BeaconId = intent.getIntExtra("BeaconId", 1);


        Toast.makeText(getApplicationContext(),beacon_id+"",Toast.LENGTH_SHORT).show();
        CourseName = (EditText) findViewById(R.id.courseName);
        InstructorName = (EditText) findViewById(R.id.instructorName);
        Description = (EditText) findViewById(R.id.description);
        txt_start_date_time = (TextView) findViewById(R.id.txt_start_date_time);
        txt_end_date_time = (TextView) findViewById(R.id.txt_end_date_time);
        start_date = (Button) findViewById(R.id.ChooseStartTime);
        end_date = (Button) findViewById(R.id.ChooseEndTime);

        Back = (Button) findViewById(R.id.back);
        Forward = (Button) findViewById(R.id.forward);

        CourseName.setText(CourseName_s);
        InstructorName.setText(InstructorName_s);
        Description.setText(Description_s);
        txt_start_date_time.setText(StartDateTime_s);
        txt_end_date_time.setText(EndDateTime_s);


        final View dialogView1 = View.inflate(ChangeBeaconsDetailActivity.this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog1 = new AlertDialog.Builder(ChangeBeaconsDetailActivity.this).create();

        dialogView1.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                datePicker= (DatePicker) dialogView1.findViewById(R.id.date_picker);
                timePicker= (TimePicker) dialogView1.findViewById(R.id.time_picker);

                int y= timePicker.getCurrentHour();
                String x = ""+timePicker.getCurrentHour();
                if (y<10)
                    x ="0" + x;

                txt_start_date_time.setText(datePicker.getYear()+"-"+((datePicker.getMonth())+1)+"-"+datePicker.getDayOfMonth()+"T"+ x+":"+timePicker.getCurrentMinute()+":00");
                ChooseStartTime_s = datePicker.getYear()+"-"+((datePicker.getMonth())+1)+"-"+datePicker.getDayOfMonth()+"T"+ x+":"+timePicker.getCurrentMinute()+":00";
                //ChooseStartTime_s =datePicker.getMonth()+""+datePicker.getDayOfMonth()+""+datePicker.getYear()+"T"+ timePicker.getCurrentHour()+""+timePicker.getCurrentMinute();
                alertDialog1.dismiss();

                Toast.makeText(getApplication().getApplicationContext(),ChooseStartTime_s+"",Toast.LENGTH_SHORT ).show();
            }});



        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog1.setView(dialogView1);
                alertDialog1.show();
            }
        });
        final View dialogView_endTime = View.inflate(ChangeBeaconsDetailActivity.this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog_endTime = new AlertDialog.Builder(ChangeBeaconsDetailActivity.this).create();

        dialogView_endTime.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker datePicker = (DatePicker) dialogView_endTime.findViewById(R.id.date_picker);
                TimePicker timePicker = (TimePicker) dialogView_endTime.findViewById(R.id.time_picker);
                //timePicker.setIs24HourView(DateFormat.is24HourFormat(AddBeaconsDetailActivity.this));

              /*  Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute());*/
                int y= timePicker.getCurrentHour();
                String x = ""+timePicker.getCurrentHour();
                if (y<10)
                    x ="0" + x;
                // ChooseEndTime_s =datePicker.getMonth()+""+datePicker.getDayOfMonth()+""+datePicker.getYear()+"T"+ timePicker.getCurrentHour()+""+timePicker.getCurrentMinute();

                txt_end_date_time.setText(datePicker.getYear()+"-"+((datePicker.getMonth())+1)+"-"+datePicker.getDayOfMonth()+"T"+ x+":"+timePicker.getCurrentMinute()+":00");
                ChooseEndTime_s =datePicker.getYear()+"-"+((datePicker.getMonth())+1)+"-"+datePicker.getDayOfMonth()+"T"+ x+":"+timePicker.getCurrentMinute()+":00";
                alertDialog_endTime.dismiss();

                Toast.makeText(getApplication().getApplicationContext(),ChooseEndTime_s+"",Toast.LENGTH_SHORT ).show();
            }});

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog_endTime.setView(dialogView_endTime);
                alertDialog_endTime.show();
            }
        });


        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                update(id,CourseName.getText().toString(),Description.getText().toString(),txt_end_date_time.getText().toString(),InstructorName.getText().toString(),txt_start_date_time.getText().toString(),BeaconId);
                Toast.makeText(getApplicationContext(),id+"beacoid="+BeaconId,Toast.LENGTH_LONG).show();
            }

        });


    }


    public void update(final int id,final String CourseName_s, final String Description_s,final String EndDateTime_s,final String InstructorName_s,final String StartDateTime_s,final int BeaconId) {
;

        final String URL = "http://bequiet.azurewebsites.net/api/contentapi/";

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest putRequest = new StringRequest(Request.Method.PUT, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Toast.makeText(getApplicationContext(), "Update Success", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Toast.makeText(getApplicationContext(), "Error!!", Toast.LENGTH_SHORT).show();
            }
        }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Id", ""+id);
                params.put("CourseName", CourseName_s);
                params.put("Description", Description_s);
                params.put("EndDateTime", EndDateTime_s);
                params.put("InstructorName", InstructorName_s);
                params.put("StartDateTime", StartDateTime_s);
                params.put("BeaconId", ""+BeaconId);


                Log.d("update",id+CourseName_s+Description_s+EndDateTime_s+InstructorName_s+StartDateTime_s+BeaconId);


                return params;
            }

        };

        queue.add(putRequest);
    }

    public void insert(){

        final String URL = "http://bequiet.azurewebsites.net/api/contentapi/";

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest putRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Toast.makeText(getApplicationContext(), " insert oldu", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
                Toast.makeText(getApplicationContext(), "hata", Toast.LENGTH_SHORT).show();
            }
        }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Id", "1");
                params.put("CourseName", CourseName_s);
                params.put("Description", Description_s);
                params.put("EndDateTime", EndDateTime_s);
                params.put("InstructorName", InstructorName_s);
                params.put("StartDateTime", StartDateTime_s);
                params.put("BeaconId", ""+BeaconId);


                return params;
            }

        };

        queue.add(putRequest);


    }





}
