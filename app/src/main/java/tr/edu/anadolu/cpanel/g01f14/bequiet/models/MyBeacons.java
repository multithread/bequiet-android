package tr.edu.anadolu.cpanel.g01f14.bequiet.models;

/**
 * Created by Tuncer on 24.12.2014.
 */
public class MyBeacons {


    private int BeaconID;
    private String Message;
    private String Place;

    public MyBeacons() {
    }

    public MyBeacons(int beaconID, String place, String message) {
        BeaconID = beaconID;
        Message = message;
        Place = place;
    }

    public void setBeaconID(int beaconID) {
        BeaconID = beaconID;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public void setPlace(String place) {
        Place = place;
    }

    public int getBeaconID() {
        return BeaconID;
    }

    public String getMessage() {
        return Message;
    }

    public String getPlace() {
        return Place;
    }
}
