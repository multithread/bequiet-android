package tr.edu.anadolu.cpanel.g01f14.bequiet.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import tr.edu.anadolu.cpanel.g01f14.bequiet.MainActivity;
import tr.edu.anadolu.cpanel.g01f14.bequiet.R;

/**
 * Created by Tuncer on 21.12.2014.
 */
public class SearchBeacon extends Service {
    private static final String ACTION="android.bluetooth.adapter.action.STATE_CHANGED";

    private BroadcastReceiver Receiver;
int state;
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

       // startService(new Intent(getApplicationContext(), FindBeaconService.class));
        final IntentFilter theFilter = new IntentFilter();
        theFilter.addAction(ACTION);
        this.Receiver = new BroadcastReceiver() {


            @Override
            public void onReceive(Context context,final Intent intent) {


            }
        };
        // Registers the receiver so that your service will listen for
        // broadcasts
        this.registerReceiver(this.Receiver, theFilter);

    }




    private void postNotification(String msg) {
        Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
        Bundle b = new Bundle();
        b.putString("notif","notif");
        notifyIntent.putExtras(b);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(
                getApplicationContext(),
                0,
                new Intent[]{notifyIntent},
                PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.beacon_gray)
                .setContentTitle(""+msg)
                .setContentText(""+msg)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;

        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(1, notification);

    }





    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(this.Receiver);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Tuncer", "entered in startMonitoring " );

        return START_STICKY;
    }


}
