package tr.edu.anadolu.cpanel.g01f14.bequiet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tr.edu.anadolu.cpanel.g01f14.bequiet.adapters.AdminBeaconListAdapter;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.MyBeacons;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.staticFields;
import tr.edu.anadolu.cpanel.g01f14.bequiet.models.BeaconInformation;
/**
 * Created by Tuncer on 12.1.2015.
 */
public class GetAllBeaconsWithIdActivity extends Activity {
    TextView tw;
    ListView list;
    int beacon_id;
    ArrayList<MyBeacons> beacons;
    ArrayList<BeaconInformation> BeaconInformation;
    AdminBeaconListAdapter beacons_adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_all_beaconswith_id_activity);

        beacons = new ArrayList<MyBeacons>();
        BeaconInformation = new ArrayList<BeaconInformation>();

        Intent intent = getIntent();
        beacon_id = intent.getIntExtra(staticFields.EXTRAS_BEACON_ID,0);



        list = (ListView) findViewById(R.id.beacons_list);
        tw = (TextView) findViewById(R.id.search);
        list.setEmptyView(tw);


        getBeaconsAndId();
        beacons_adapter = new AdminBeaconListAdapter(getApplicationContext(),beacons);

        list.setAdapter(beacons_adapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(GetAllBeaconsWithIdActivity.this, ChangeBeaconsDetailActivity.class);
                intent.putExtra("Id", BeaconInformation.get(position).getBeacon_id());
                intent.putExtra("CourseName", BeaconInformation.get(position).getCourseName());
                intent.putExtra("Description", BeaconInformation.get(position).getDescription());
                intent.putExtra("EndDateTime", BeaconInformation.get(position).getEndDateTime());
                intent.putExtra("InstructorName", BeaconInformation.get(position).getInstructorName());
                intent.putExtra("StartDateTime", BeaconInformation.get(position).getStartDateTime());
                intent.putExtra("BeaconId", BeaconInformation.get(position).getBeaconId());
                startActivity(intent);
            }
        });


        Toast.makeText(getApplicationContext(), beacon_id + "", Toast.LENGTH_SHORT).show();

    }



    void getBeaconsAndId(){

        final ProgressDialog mProgressDialog;

        mProgressDialog = new ProgressDialog(GetAllBeaconsWithIdActivity.this);
        mProgressDialog.setTitle("Service Connection");
        mProgressDialog.setMessage("Reaching...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.show();

        String URL1 = "http://bequiet.azurewebsites.net/api/contentapi?beaconid="+beacon_id;
        JsonArrayRequest req = new JsonArrayRequest(URL1, new Response.Listener<JSONArray>(){

            @Override
            public void onResponse(JSONArray response) {
                try {

                    Log.d("allbeacon", "?");
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject obj = response.getJSONObject(i);
                        MyBeacons beacon = new MyBeacons(obj.getInt("BeaconId"), obj.getString("CourseName"),obj.getString("Description") );
                        beacons.add(beacon);
                        BeaconInformation inf = new BeaconInformation(obj.getInt("Id"),obj.getString("CourseName"),obj.getString("Description"),obj.getString("EndDateTime"),obj.getString("InstructorName"),obj.getString("StartDateTime"),obj.getInt("BeaconId"));
                        BeaconInformation.add(inf);
                    }
                    beacons_adapter.notifyDataSetChanged();
                    mProgressDialog.dismiss();



                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("allbeacon", "hata");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        ApplicationController.getInstance().addToRequestQueue(req);


    }

}
