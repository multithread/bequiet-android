package tr.edu.anadolu.cpanel.g01f14.bequiet.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.estimote.sdk.Region;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;


import tr.edu.anadolu.cpanel.g01f14.bequiet.ApplicationController;
import tr.edu.anadolu.cpanel.g01f14.bequiet.MainActivity;
import tr.edu.anadolu.cpanel.g01f14.bequiet.R;
import tr.edu.anadolu.cpanel.g01f14.bequiet.fragments.BeaconContentFragment;
import tr.edu.anadolu.cpanel.g01f14.bequiet.fragments.HomeBeaconFragment;

/**
 * Created by Tuncer on 21.12.2014.
 */
public class FindBeaconService extends Service {

    BeaconManager beaconManager;
    String URL;
    static final Region[] BEACONS = new Region[] {
            new Region("beacon3", "b9407f30f5f8466eaff925556b57fe6d", null, null)
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Tuncer", "entered in startMonitoring ");
        startMonitoring();
        return START_STICKY;
    }

    private void startMonitoring() {
        if (beaconManager == null) {
            beaconManager = new BeaconManager(this);

            // Configure verbose debug

            /**
             * Scanning
             */
            beaconManager.setBackgroundScanPeriod(TimeUnit.SECONDS.toMillis(1), 1);

            beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
                @Override
                public void onEnteredRegion(Region region, List<Beacon> paramList) {
                    //notifyEnterRegion(12);
                    if (paramList != null && !paramList.isEmpty()) {
                        Beacon beacon = paramList.get(0);
                        Utils.Proximity proximity = Utils.computeProximity(beacon);
                       // if (Utils.computeAccuracy(beacon) <= 2) {
                        if(calculateDistance(beacon)!=2){
                            Log.d("Tuncer", "entered in region " + region.getProximityUUID());

                            URL  = "http://bequiet.azurewebsites.net/api/contentapi/"+beacon.getMajor();
                            Log.d("Tuncer", "url"+ URL);
                            JsonObjectRequest req = new JsonObjectRequest(URL, null,
                                    new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                postNotification(response.getString("InstructorName"),response.getString("Description"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    VolleyLog.e("Error: ", error.getMessage());


                                }
                            });

                            ApplicationController.getInstance().addToRequestQueue(req);


                        }
                    }
                }

                @Override
                public void onExitedRegion(Region region) {

                }
            });


            beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                @Override
                public void onServiceReady() {
                    try {
                        Log.d("Tuncer", "connected");
                        for (Region region : BEACONS) {
                            beaconManager.startMonitoring(region);
                        }
                    } catch (RemoteException e) {
                        Log.d("Tuncer", "Error while starting monitoring");
                    }
                }
            });
        }
    }
    private void postNotification(String msg,String msg2) {
        Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
       // Bundle b = new Bundle();
       // b.putString("notif","notif");
       // notifyIntent.putExtras(b);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(
                getApplicationContext(),
                0,
                new Intent[]{notifyIntent},
                PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.beacon_gray)
                .setContentTitle(""+msg)
                .setContentText(""+msg2)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;

        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(1, notification);

    }
    public double calculateDistance(Beacon beacon) {
        return Math.min(Utils.computeAccuracy(beacon), 2);
    }
}
