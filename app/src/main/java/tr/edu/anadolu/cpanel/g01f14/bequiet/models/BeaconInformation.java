package tr.edu.anadolu.cpanel.g01f14.bequiet.models;

/**
 * Created by Tuncer on 12.1.2015.
 */
public class BeaconInformation {

    private int beacon_id;
    private String CourseName;
    private String Description;
    private String EndDateTime;
    private String InstructorName;
    private String StartDateTime;
    private int BeaconId;


    public BeaconInformation(int beacon_id, String courseName, String description, String endDateTime, String instructorName, String startDateTime, int beaconId) {
        this.beacon_id = beacon_id;
        CourseName = courseName;
        Description = description;
        EndDateTime = endDateTime;
        InstructorName = instructorName;
        StartDateTime = startDateTime;
        BeaconId = beaconId;
    }

    public int getBeacon_id() {
        return beacon_id;
    }

    public void setBeacon_id(int beacon_id) {
        this.beacon_id = beacon_id;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEndDateTime() {
        return EndDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        EndDateTime = endDateTime;
    }

    public String getInstructorName() {
        return InstructorName;
    }

    public void setInstructorName(String instructorName) {
        InstructorName = instructorName;
    }

    public String getStartDateTime() {
        return StartDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        StartDateTime = startDateTime;
    }

    public int getBeaconId() {
        return BeaconId;
    }

    public void setBeaconId(int beaconId) {
        BeaconId = beaconId;
    }
}
