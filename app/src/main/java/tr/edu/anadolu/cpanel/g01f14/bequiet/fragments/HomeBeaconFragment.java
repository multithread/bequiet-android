package tr.edu.anadolu.cpanel.g01f14.bequiet.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import tr.edu.anadolu.cpanel.g01f14.bequiet.MainActivity;
import tr.edu.anadolu.cpanel.g01f14.bequiet.R;
import tr.edu.anadolu.cpanel.g01f14.bequiet.adapters.DeviceListAdapter;

/**
 * Created by Tuncer on 14.12.2014.
 */
public class HomeBeaconFragment extends Fragment {
    private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);
    static final Region[] BEACONS = new Region[] {
            new Region("beacon3", "b9407f30f5f8466eaff925556b57fe6d", null, null)
    };

    private static final int REQUEST_ENABLE_BT = 1234;

    BeaconManager beaconManager;
   DeviceListAdapter adapter;
    ListView list;
    ArrayList<Beacon> Found_beacons;
    TextView tw;
    public HomeBeaconFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Found_beacons = new ArrayList<Beacon>();
        View rootView = inflater.inflate(R.layout.home_beacon_fragment, container, false);
        list = (ListView) rootView.findViewById(R.id.device_list);
        tw = (TextView) rootView.findViewById(R.id.search);
        list.setEmptyView(tw);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                FragmentManager fragmentManager2 = getFragmentManager();
                FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                BeaconContentFragment fragment2 = new BeaconContentFragment();
              //  fragment2.newInstance(""+Found_beacons.get(position).getMajor(),"");
                Bundle bundle = new Bundle();
                bundle.putString("param1", ""+Found_beacons.get(position).getMajor());
                fragment2.setArguments(bundle);
                fragmentTransaction2.addToBackStack(null);
                fragmentTransaction2.hide(HomeBeaconFragment.this);
                fragmentTransaction2.replace(R.id.frame_container, fragment2);
                fragmentTransaction2.commit();
            }
        });



        adapter = new DeviceListAdapter(getActivity().getApplicationContext());
        list.setAdapter(adapter);
        beaconManager = new BeaconManager(getActivity().getApplicationContext());
        beaconManager.setBackgroundScanPeriod(5, 0);
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                // Note that results are not delivered on UI thread.
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.replaceWith(beacons);
                        Found_beacons.clear();
                        Found_beacons.addAll(beacons);
                    }
                });


//                Toast.makeText(getActivity().getApplicationContext(), "tunker", Toast.LENGTH_LONG).show();

            }



        });



        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> beacons) {
                //postNotification("haydaaaa");

            }

            @Override
            public void onExitedRegion(Region region) {

            }
        });

        return rootView;
    }

    public void setFound_beacons(List<Beacon> beacons) {
        this.Found_beacons.clear();
        this.Found_beacons.addAll(beacons);

    }

    @Override
    public void onDestroy() {
        beaconManager.disconnect();

        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();

        // Check if device supports Bluetooth Low Energy.
        if (!beaconManager.hasBluetooth()) {
            Toast.makeText(getActivity().getApplicationContext(), "Device does not have Bluetooth Low Energy", Toast.LENGTH_LONG).show();
            return;
        }

        // If Bluetooth is not enabled, let user enable it.
        if (!beaconManager.isBluetoothEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            connectToService();
        }
    }

    @Override
    public void onStop() {
        try {
            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
        } catch (RemoteException e) {

        }

        super.onStop();
    }

    private void connectToService() {
       // getActionBar().setSubtitle("Scanning...");
        adapter.replaceWith(Collections.<Beacon>emptyList());
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                try {
                    beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
                    Toast.makeText(getActivity().getApplicationContext(), "start ranging",
                            Toast.LENGTH_LONG).show();
                } catch (RemoteException e) {
                    Toast.makeText(getActivity().getApplicationContext(), "Cannot start ranging, something terrible happened",
                            Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                connectToService();
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Bluetooth not enabled", Toast.LENGTH_LONG).show();
                //getActionBar().setSubtitle("Bluetooth not enabled");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }





    private void postNotification(String msg) {
        Intent notifyIntent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(
                getActivity().getApplicationContext(),
                0,
                new Intent[]{notifyIntent},
                PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(getActivity().getApplicationContext())
                .setSmallIcon(R.drawable.beacon_gray)
                .setContentTitle(""+msg)
                .setContentText(""+msg)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;

        NotificationManager nManager = (NotificationManager) getActivity().getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(1, notification);
        // notificationManager.notify(123, notification);

    }

}
