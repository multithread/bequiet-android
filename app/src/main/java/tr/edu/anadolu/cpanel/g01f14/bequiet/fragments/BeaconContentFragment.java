package tr.edu.anadolu.cpanel.g01f14.bequiet.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import tr.edu.anadolu.cpanel.g01f14.bequiet.ApplicationController;
import tr.edu.anadolu.cpanel.g01f14.bequiet.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BeaconContentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BeaconContentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BeaconContentFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //private TextView textView2;
    private TextView textView4;
    private TextView textView6;
    private TextView textView8;
    private TextView textView10;
    private TextView textView12;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BeaconContentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BeaconContentFragment newInstance(String param1, String param2) {
        BeaconContentFragment fragment = new BeaconContentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public BeaconContentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            Log.d("Tuncer", "url fragment alınan : " + mParam1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_beacon_content, container, false);


       // textView2 = (TextView) view.findViewById(R.id.textView2);
        textView4 = (TextView) view.findViewById(R.id.textView4);
        textView6 = (TextView) view.findViewById(R.id.textView6);
        textView8 = (TextView) view.findViewById(R.id.textView8);
        textView10 = (TextView) view.findViewById(R.id.textView10);
        textView12 = (TextView) view.findViewById(R.id.textView12);


        Bundle n = getArguments();
        if (n != null) {
            mParam1 = n.getString("param1");
            Log.d("Tuncer", "url fragment alınan2 : " + mParam1);
            mParam2 = n.getString(ARG_PARAM2);
        }else
            Log.d("Tuncer", "url fragment alınmadı : " + mParam1);


        final String URL = "http://bequiet.azurewebsites.net/api/contentapi/"+mParam1;

        JsonObjectRequest req = new JsonObjectRequest(URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //textView2.setText("");
                            textView4.setText(response.getString("CourseName"));
                            textView6.setText(response.getString("InstructorName"));
                            textView8.setText(response.getString("Description"));
                            textView10.setText(response.getString("StartDateTime"));
                            textView12.setText(response.getString("EndDateTime"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());

                Toast.makeText(getActivity().getApplicationContext(), "There in no Activity", Toast.LENGTH_LONG).show();
            }
        });

        ApplicationController.getInstance().addToRequestQueue(req);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
