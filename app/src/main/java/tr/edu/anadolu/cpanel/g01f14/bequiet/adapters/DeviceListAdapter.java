package tr.edu.anadolu.cpanel.g01f14.bequiet.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.estimote.sdk.Beacon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import tr.edu.anadolu.cpanel.g01f14.bequiet.ApplicationController;
import tr.edu.anadolu.cpanel.g01f14.bequiet.R;

/**
 * Created by Tuncer on 18.12.2014.
 */
public class DeviceListAdapter  extends BaseAdapter {


    private ArrayList<Beacon> beacons;
    private LayoutInflater inflater;

    public DeviceListAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.beacons = new ArrayList<Beacon>();
    }



    public void replaceWith(Collection<Beacon> newBeacons) {
        this.beacons.clear();
        this.beacons.addAll(newBeacons);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return beacons.size();
    }

    @Override
    public Beacon getItem(int position) {
        return beacons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflateIfRequired(convertView, position, parent);
        bind(getItem(position), convertView,position);


        return convertView;
    }
    private void bind(Beacon beacon, View view,int position) {
        ViewHolder  holder  = (ViewHolder) view.getTag();

        if(position % 2 == 0){
            holder.layout.setBackgroundResource(R.drawable.layout_shape);
        }else
            holder.layout.setBackgroundResource(R.drawable.layout_shape_pass);

        getDetails(beacon, holder);

    }
    private View inflateIfRequired(View view, int position, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.device_list_items, null,false);
            view.setTag(new ViewHolder(view));
        }
        return view;
    }

    static class ViewHolder {
        final TextView classTextView;
        final TextView messageTextView;
        final ImageView beaconImageView;
        final RelativeLayout layout;


        ViewHolder(View view) {
            beaconImageView = (ImageView) view.findViewWithTag("image");
            classTextView = (TextView) view.findViewWithTag("class");
            messageTextView = (TextView) view.findViewWithTag("message");
            layout =(RelativeLayout)view.findViewWithTag("layout");
        }
    }

    void getDetails(Beacon beacon,final ViewHolder  holder){

        final String URL = "http://bequiet.azurewebsites.net/api/contentapi/"+beacon.getMajor();
        Log.d("Tuncer", "url adapter" + URL);
        JsonObjectRequest req = new JsonObjectRequest(URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("response adap",response.toString());
                            holder.beaconImageView.setImageResource(R.drawable.beacon_gray);
                            holder.classTextView.setText( response.getString("InstructorName"));
                            holder.messageTextView.setText( response.getString("Description"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            holder.beaconImageView.setImageResource(R.drawable.beacon_gray);
                            holder.classTextView.setText( "InstructorName");
                            holder.messageTextView.setText( "Description");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                holder.beaconImageView.setImageResource(R.drawable.beacon_gray);
                holder.classTextView.setText( "InstructorName");
                holder.messageTextView.setText( "Description");

            }
        });
        ApplicationController.getInstance().addToRequestQueue(req);
    }
}
